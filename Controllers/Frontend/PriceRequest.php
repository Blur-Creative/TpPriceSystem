<?php

class Shopware_Controllers_Frontend_PriceRequest extends Enlight_Controller_Action
{
    public function indexAction()
    {
        die('Hello world');
    }

    public function fetchFormAction()
    {
        $session = $this->get( 'session' );

        if ( $session->sUserId ) {
            $this->View()->assign( 'firstName', $session->userInfo['firstname'] );
            $this->View()->assign( 'lastName', $session->userInfo['lastname'] );
            $this->View()->assign( 'email', $session->userInfo['email'] );
        }

        $this->View()->assign( 'productOrdernumber', $this->Request()->getParam( 'productOrdernumber' ) );
    }

    public function sendAction()
    {
        $productOrdernumber = $this->Request()->getParam( 'orderNumber' );
        $firstName = $this->Request()->getParam( 'firstName' );
        $lastName = $this->Request()->getParam( 'lastName' );
        $email = $this->Request()->getParam( 'email' );
        $emailOwner = $this->get( 'Shopware_Components_Config')->get('mail');

        /**
         * @var \Shopware_Components_TemplateMail
         */
        $templateMail = $this->get( 'templatemail' );

        $mailVariables = [
            'orderNumber' => $productOrdernumber,
            'firstName' => $firstName,
            'lastName' => $lastName,
            'email' => $email,
        ];

        try {

            $priceRequestMail = $templateMail->createMail( 'TpPriceRequest', $mailVariables );

            $priceRequestMail->addTo( $email );
            # set shop owner as bcc
            $priceRequestMail->addBcc( $emailOwner );
            $priceRequestMail->send();
            

            # load this template on success response
            $this->View()->loadTemplate( 'frontend/price_request/send_success.tpl' );

        } catch ( \Exception $error ) {

            # load this template on error response
            $this->View()->loadTemplate( 'frontend/price_request/send_error.tpl' );
        }
    }
}