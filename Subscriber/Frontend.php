<?php

namespace TpPriceSystem\Subscriber;

use Enlight\Event\SubscriberInterface;
use Shopware\Bundle\AttributeBundle\Service\DataLoader;

class Frontend implements SubscriberInterface {

    /**
     * @var string
     */
    protected $pluginDirectory;

    protected $session;

    /**
     * @var DataLoader
     */
    protected $attributeDataLoader;

    /**
     * @var string
     */    
    protected $productsWhitelistSeperator = ';';

    /**
     * @var bool
     */    
    protected $priceRequest = true;

    public function __construct(
        string $pluginDirectory,
        $session,
        DataLoader $attributeDataLoader
    )
    {
        $this->pluginDirectory = $pluginDirectory;
        $this->session = $session;
        $this->attributeDataLoader = $attributeDataLoader;
    }

    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PreDispatch_Frontend' => 'onFrontend',
            'Legacy_Struct_Converter_Convert_List_Product' => 'viewProduct',
            'Legacy_Struct_Converter_Convert_Product' => 'viewProduct'
        ];
    }

    public function onFrontend( $args )
    {
        $args->getSubject()->View()->addTemplateDir($this->pluginDirectory . '/Resources/views');
    }

    public function viewProduct( $args )
    {
        $productData = $args->getReturn();

        if ( !( !empty( $this->getProductWhitelist() ) && in_array( $productData['ordernumber'], $this->getProductWhitelist() ) ) ) {
            return $this->setPriceRestrictions( $productData );
        }
    }

    protected function setPriceRestrictions( $productData ): array
    {
        $productData['hidePrice'] = true;
        $productData['priceRequest'] = $this->priceRequest;
        $productData['prices'] = null;
        $productData['pseudoprice_numeric'] = null;
        $productData['price_numeric'] = null;
        $productData['pseudoprice'] = null;
        $productData['price'] = null;
        $productData['referenceprice'] = null;

        return $productData;
    }

    protected function getProductWhitelist(): ?array
    {
        if ( $this->session->sUserId && !empty( $this->attributeDataLoader->load( 's_user_attributes', $this->session->sUserId )[ 'allowed_article_permission' ] ) ) {
            return explode( $this->productsWhitelistSeperator, $this->attributeDataLoader->load( 's_user_attributes', $this->session->sUserId )[ 'allowed_article_permission' ] );    
        } else {
            return null;
        }
    } 

}