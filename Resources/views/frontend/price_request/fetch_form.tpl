{namespace name='frontend/price_request/form'}

<div class="price-request__form-title">
    {s name='priceRequestTitle'}Preisanfrage für Artikel{/s} {$productOrdernumber}
</div>
<div class="price-request__form-description">
    {s name='requestDescription'}Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.{/s}
</div>
<div class="price-request__form-container">

    <form action="{url controller='price_request' action='send'}" 
        data-price-request="form"
        class="price-request__form">
        
        <input type="hidden" name="orderNumber" value="{$productOrdernumber}" />
        
        <div class="price-request__form-firstname">
            <input type="text" 
                name="firstName" 
                id="firstName" 
                class="price-request__input price-request__input-firstname"
                placeholder="{s name='formFirstname'}Vorname{/s}"
                {if $firstName} value="{$firstName}"{/if} />
        </div>

        <div class="price-request__form-lastname">
            <input type="text" 
                   name="lastName" 
                   id="lastName" 
                   class="price-request__input price-request__input-lastname"
                   placeholder="{s name='formLastname'}Nachname{/s}"
                   {if $lastName} value="{$lastName}"{/if} />
        </div>

        <div class="price-request__form-email">
            <input type="email" 
                   name="email" 
                   id="email" 
                   class="price-request__input price-request__input-email"
                   placeholder="{s name='formEmail'}E-Mail Adresse{/s}"
                   {if $email} value="{$email}"{/if} required />
        </div>

        <div class="price-request__form-actions">

            <button type="submit" class="btn is--primary">
                {s name='formSubmitButtonLabel'}Anfrage senden{/s}
            </button>        
        </div>
    </form>
</div>