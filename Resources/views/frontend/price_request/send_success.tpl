{namespace name='frontend/price_request/success'}

<div class="price-request__form-title">
    {s name='fromTitle'}Danke für Ihre Anfrage{/s}
</div>
<div class="price-request__form-container">
    {s name='successMessage'}Danke{/s}
</div>