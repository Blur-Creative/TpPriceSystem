<div class="detail-price-request">

    <button type="button" 
            data-price-request="modal"
            data-price-request-form="{url controller='price_request' action='fetch_form'}"
            data-price-request-product="{$sArticle.ordernumber}"
            class="btn is--primary detail-price-request__button">
        Preis auf Anfrage
    </button>
</div>