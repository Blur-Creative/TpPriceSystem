{extends file='parent:frontend/detail/content/buy_container.tpl'}

{* Include buy button and quantity box *}
{block name="frontend_detail_index_buybox"}
    {if $sArticle.priceRequest}
        {block name="frontend_detail_tp_price_request_include"}
            {include file="frontend/detail/content/price-request-button.tpl"}
        {/block}
    {else}
        {$smarty.block.parent}
    {/if}
{/block}