{extends file='parent:frontend/listing/product-box/box-basic.tpl'}

{* Product price - Default and discount price *}
{block name='frontend_listing_box_article_price'}
    {if !$sArticle.hidePrice}
        {$smarty.block.parent}
    {/if}
{/block}