(function ($, window) {
    'use strict';

    $.plugin('tpPriceRequestModal', {

        defaults: {
            priceRequestForm: ''
        },

        /**
         * Initializes the plugin, applies addition data attributes and
         * registers events for clicking the target element.
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this;

            me.priceRequestFormUrl = me.$el.data( 'priceRequestForm' );

            me.productOrdernumber = me.$el.data( 'priceRequestProduct' );

            console.log(me.priceRequestFormUrl);

            me.applyDataAttributes();

            me.registerEvents();


            $.publish('plugin/tpPriceRequestModal/initEnd', [ me ]);
        },

        registerEvents: function () {
            var me = this;

            me.$el.on( 'click', $.proxy( me.openModal, me ) );
        },

        openModal: function () {
            var me = this;

            // Ajax request to fetch available addresses
            $.ajax({
                url: me.priceRequestFormUrl,
                data: {
                    productOrdernumber: me.productOrdernumber
                },
                success: function(data) {

                    $.modal.open(data);

                    me._registerPlugins(); 
                }
            });
        },

        _registerPlugins: function () {
            window.StateManager.addPlugin('*[data-price-request="form"]', 'tpPriceRequestForm');           
        }
    });
})(jQuery, window);

window.StateManager.addPlugin('[data-price-request="modal"]', 'tpPriceRequestModal');