(function ($, window) {
    'use strict';

    $.plugin( 'tpPriceRequestForm', {

        defaults: {
        },

        /**
         * Initializes the plugin, applies addition data attributes and
         * registers events for clicking the target element.
         *
         * @public
         * @method init
         */
        init: function () {
            var me = this;

            me.applyDataAttributes();

            me.registerEvents();

            $.publish('plugin/tpPriceRequestForm/initEnd', [ me ]);
        },

        registerEvents: function () {
            var me = this;

            console.log( 'tpPriceRequestForm' );

            me.$el.on( 'submit', $.proxy( me.formSubmit, me ) );
        },

        formSubmit: function ( event ) {
            var me = this,
                formAction = me.$el.attr( 'action' ),
                formData = me.$el.serialize();

            event.preventDefault();

            // Ajax request to fetch available addresses
            $.ajax({
                url: formAction,
                data: formData,
                method: 'POST',
                success: function(result) {

                    $.modal.setContent( result );
                    console.log(result);
                }
            });
        }
    });
})(jQuery, window);

//window.StateManager.addPlugin('[data-price-request="form"]', 'tpPriceRequestForm');