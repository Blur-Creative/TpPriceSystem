<?php

namespace TpPriceSystem;

use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;
use Shopware\Components\Model\ModelManager;
use Shopware\Models\Mail\Mail;

class TpPriceSystem extends Plugin
{
    public const MAIL_TEMPLATE_NAME = 'TpPriceRequest';
    public const MAIL_FROM_MAIL = '{config name=mail}';
    public const MAIL_FROM_NAME = '{config name=shopName}';

    public function install(InstallContext $context)
    {
        $crudService = $this->container->get( 'shopware_attribute.crud_service' );

        if( !$crudService->get( 's_user_attributes' , 'allowed_article_permission' ) ) {    

            $crudService->update(
                's_user_attributes', 
                'allowed_article_permission', 
                'text',
                [
                    'label' => 'Erlaubte Artikel',
                    'displayInBackend' => true,
                    'custom' => true
                ]
            );
        }

        $metaDataCache = Shopware()->Models()->getConfiguration()->getMetadataCacheImpl();
        $metaDataCache->deleteAll();
        Shopware()->Models()->generateAttributeModels(['s_user_attributes']);

        $this->installMailTemplate();
    }

    public function uninstall(UninstallContext $context)
    {
        if ( !$context->keepUserData() ) {

            $crudService = $this->container->get( 'shopware_attribute.crud_service' );

            if( $crudService->get( 's_user_attributes' , 'allowed_article_permission' ) ) {    
    
                $crudService->delete(
                    's_user_attributes', 
                    'allowed_article_permission'
                );
            }
        }

        $this->uninstallMailTemplate();
    }

        /**
     * installMailTemplate takes care of creating the new E-Mail-Template
     */
    private function installMailTemplate(): void
    {
        $entityManager = $this->container->get('models');
        $mail = new Mail();

        // After creating an empty instance, some technical info is set
        $mail->setName( self::MAIL_TEMPLATE_NAME );
        $mail->setMailtype( Mail::MAILTYPE_USER );
        $mail->setFromMail( self::MAIL_FROM_MAIL );
        $mail->setFromName( self::MAIL_FROM_NAME );
        $mail->setIsHtml( true );

        // Now the templates basic information can be set
        $mail->setSubject( $this->mailSubject() );
        $mail->setContent( $this->mailContent() );
        $mail->setContentHtml( $this->mailContentHtml() );

        /**
         * Finally the new template can be persisted.
         *
         * transactional is a helper method which wraps the given function
         * in a transaction and executes a rollback if something goes wrong.
         * Any exception that occurs will be thrown again and, since we're in
         * the install method, shown in the backend as a growl message.
         */
        $entityManager->transactional(static function ($em) use ($mail) {
            /** @var ModelManager $em */
            $em->persist($mail);
        });
    }

        /**
     * uninstallMailTemplate takes care of removing the plugin's E-Mail-Template
     */
    private function uninstallMailTemplate(): void
    {
        $entityManager = $this->container->get('models');
        $repo = $entityManager->getRepository(Mail::class);

        // Find the mail-type we created
        $mail = $repo->findOneBy(['name' => self::MAIL_TEMPLATE_NAME]);

        $entityManager->transactional(static function ($em) use ($mail) {
            /** @var ModelManager $em */
            $em->remove($mail);
        });
    }

    private function mailSubject(): string
    {
        return 'Preisanfrage für Artikel {$ordernumber}';
    }

    private function mailContent(): string
    {
        /**
         * Notice the string:{...} in the include's file-attribute.
         * This causes the referenced config value to be loaded into
         * a string and passed on as the template's content. This works
         * because the file-attribute can accept any template resource
         * which includes paths to files and several other types as well.
         * For more information about template resources, have a look here:
         * https://www.smarty.net/docs/en/resources.string.tpl
         */
        return <<<'EOD'
            {include file="string:{config name=emailheaderplain}"}

            Danke für Ihre Preisanfrage zum Artikel {$productName}.
            Wir werden uns in Kürze bei Ihnen melden.

            {include file="string:{config name=emailfooterplain}"}
        EOD;
    }

    private function mailContentHtml(): string
    {
        return <<<'EOD'
            {include file="string:{config name=emailheaderhtml}"}

            Danke für Ihre Preisanfrage zum Artikel {$productName}.<br/>
            Wir werden uns in Kürze bei Ihnen melden.

            {include file="string:{config name=emailfooterhtml}"}
        EOD;
    }
}
